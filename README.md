# A genetic solver for knapsack 0-1 problem

Developed in OCaml for the A.I. - Functional Programming class at University of Perugia (2012).

## Usage

```
usage: ./kpacking [-v] [-p population_size] [-c crossover_chance] [-m mutation_chance] [-i iterations] -f file
  -v : verbose output
  -p : population size [default 50]
  -c : chance of crossover [default 0.95]
  -m : chance of mutation [default 1/(num objects)]
  -i : iterations [default 100]
  -g : first generation style [0, 1 or 2. Default 1 for random genes (0 = zero filled, 2 = other style random gene generation)]
  -s : optimal known solution (the program will exits if found before the last iteration)
  -f : file with problem data
  -help  Display this list of options
  --help  Display this list of options
```